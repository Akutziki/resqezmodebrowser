# ResqEzModeBrowser
1.  Open developer console from broswer by pressing F12.
2.  Go to https://resq-club.com/app/
3.  Navigate to your region by dragging and scrolling the map.
4.  Select "console".
5.  Paste code to console and press enter and then wait. Program will cast an alert if food including word "kassi" is found.
6.  Eat.

**Alerts user if Resq has items that contain word "kassi" like for example "Ruokakassi", "Iltakassi", "Leipäkassi" etc.**